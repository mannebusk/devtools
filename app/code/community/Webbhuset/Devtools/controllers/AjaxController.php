<?php

class Webbhuset_Devtools_AjaxController extends Mage_Core_Controller_Front_Action
{
    /**
     * Useless index action
     */
    public function indexAction()
    {
        $this->_validateRequest();
        $this->_forward('defaultNoRoute');
    }

    /**
     * Toggle template path hints
     */
    public function templatepathhintsAction()
    {
        $this->_validateRequest();
        $config = Mage::getModel('core/config');
        $store = Mage::app()->getStore()->getId();
        $session = Mage::getModel('customer/session');

        $value = $session->getData('devtools_templatehint');
        if ($value) {
            $value = 0;
        } else {
            $value = 1;
        }

        $session->setData('devtools_templatehint', $value);
        $config->saveConfig('dev/debug/template_hints', $value, 'stores', $store);

        if ($value) {
            echo 'ON';
        } else {
            echo 'OFF';
        }
    }

    /**
     * Clean cache (Deleteing files)
     */
    public function cleancacheAction()
    {
        $this->_validateRequest();
        $cacheDir = Mage::getBaseDir('var') . DS . 'cache' . DS;
        $files = glob($cacheDir . '*');
        foreach ($files as $dir) {
            $cacheFiles = glob($dir . DS . '*');
            foreach($cacheFiles as $cache) {
                unlink($cache);
            }
        }
        echo $this->_getRandomChuckNorrisCacheCleaningMessage();
    }


    /**
     * Only allow Ajax requests and only for developers
     *
     */
    protected function _validateRequest()
    {
        if (!$this->getRequest()->isXmlHttpRequest() || !Mage::helper('devtools')->isEnabled()) {
            $this->_forward('defaultNoRoute');
        }
    }


    /**
     * Get random Chuck Norris cache cleaning phrase
     *
     * @return string
     */
    protected function _getRandomChuckNorrisCacheCleaningMessage()
    {
        $chuck = array(
            'Chuck Norris threw a grenade and killed all cache files, then it exploded.',
            'Chuck Norris has strangled the cache with a cordless phone.',
            'Chuck Norris counted to infinity. Twice.  ... and also your cache is cleaned.',
            'What was going through the minds of all of all cache files before they died? Chuck Norris shoe.',
        );
        return $chuck[rand(0, (count($chuck) - 1))];
    }
}

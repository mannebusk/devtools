<?php 

class Webbhuset_Devtools_Block_Init extends Mage_Core_Block_Template
{
    /**
     * Check if block is allowed to output data 
     */
    protected function _toHtml()
    {
        if (Mage::helper('devtools')->IsEnabled()) {
            return parent::_toHtml();
        }
    }

    /**
     * Get layout handels as json
     */
    public function getLayoutHandles()
    {
        $handles = $this->getLayout()->getUpdate()->getHandles();
        return Mage::helper('core')->jsonEncode($handles);
    }

    /**
     * Get js URL
     */
    public function getScriptUrl($file)
    {
        $jsFolder = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_JS) . DS . 'devtools' . DS;
        return $jsFolder . $file;
    }
}

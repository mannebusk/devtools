<?php 

class Webbhuset_Devtools_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Check if Devtools is enabled
     *
     * @return bool
     */
    public function isEnabled()
    {
        $allowedIps = Mage::getStoreConfig( Mage_Core_Helper_Data::XML_PATH_DEV_ALLOW_IPS,
                                            Mage::app()->getStore()->getId());
        $devAllowed = Mage::helper('core')->isDevAllowed();
        $config = Mage::getStoreConfig('webbhuset_devtools/general');

        if ($config['enable'] && $allowedIps && $devAllowed) {
            return true;
        }
        return false;
    }
}

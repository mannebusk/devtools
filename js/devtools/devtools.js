
var Devtools = function(options){
    that = this;
    
    /**
     * Set up devtools
     */
    that.init = function() {
        console.log('Webbhuset Devtools Activated');
        that.showLayoutHandles();
        that.registerKeyBinds();
    };

    /**
     * Register key bindings
     */
    that.registerKeyBinds = function() {
        var t = new Keybinding('<shift>t', that.templatePathHints);
        var cc = new Keybinding('<shift>cc', that.cleanCache);
        var h = new Keybinding('<shift>h', that.showLayoutHandles);
    };

    /**
     * Toggle template path hints
     */
    that.templatePathHints = function() {
        new Ajax.Request(options.baseUrl + '/devtools/ajax/templatepathhints/', {
            onSuccess: function(response) {
                var msg = 'Template Path Hints ' + response.responseText;
                console.log(msg);
            },
            onError: function(response) {
                console.log(response);
            }
        });
    };

    /**
     * Clean out the cache
     */
    that.cleanCache = function() {
        new Ajax.Request(options.baseUrl + '/devtools/ajax/cleancache/', {
            onSuccess: function(response) {
                var msg = 'Cache Cleaned';
                console.log(response.responseText);
            },
            onError: function(response) {
                console.log(response);
            }
        });
    };

    /**
     * Show layout handles
     */
    that.showLayoutHandles = function() {
        if (typeof options.handles !== 'undefined') {
            console.log(options.handles);
        }
    };

    that.init();
    return that;
};
